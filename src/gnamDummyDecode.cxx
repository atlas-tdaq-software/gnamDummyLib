//$Id$

// GNAM: global include file for libraries
#include <gnam/gnamutils/GnamUtils.h>

// TDAQ: OKS DB entry point
#include <config/Configuration.h>

// TDAQ: for DB object casting
#include <dal/util.h>

// MY CLASS: definition of the class correspoding to my event
#include "gnamDummyLib/MyClass.h"

// MY DAL: definition of the DB object correspoding to my library
#include "gnamDummyLibdal/gnamDummyDecodeLib.h"

// MY EXCEPTION: definitions of exceptions
#include "gnamDummyLib/GnamDummyException.h"

#include <stdlib.h>
#include <errno.h>

// GNAM
/*
  Use *one* instance for each one of your data classes; "SetBranch" will
  record the address of this instance and it cannot change until next run.
*/
static MyClass * MyObject = NULL;

unsigned int rodSizeCheck;

// GNAM
/*
  "initDB" function: called once just after the library has been loaded
  in case of configuration from OKS DB.
  You should collect your configuration parameters from the DB.
*/
extern "C" void
initDB (Configuration * confDB, const gnamdal::GnamLibrary * library)
{
    ERS_DEBUG (2, "Starting");

    // In order to read your configuration parameters, you 
    // need to downcast the abstract object "library" to 
    // the type of your library 
    const gnamDummyLibdal::gnamDummyDecodeLib * dummyLib =
        confDB->cast<gnamDummyLibdal::gnamDummyDecodeLib,
        gnamdal::GnamLibrary>(library);
  
    rodSizeCheck=dummyLib->get_RodSizeCheck();

  ERS_DEBUG (2, "Selected ROD: " << rodSizeCheck);

  MyObject = new MyClass();
  
  // GNAM
  // Use GNAM_BRANCH_REGISTER with 2 parameters:
  /*
    The 1st parameter is the name of the branch.
    The 2nd parameter is the pointer to the object to be stored. 
  */
  try {
    GNAM_BRANCH_REGISTER ("MyClassBranch", MyObject);
  }
  catch (daq::gnamlib::AlreadyExistingBranch &exc) {
    throw daq::gnamlib::CannotRegisterBranch (ERS_HERE, "MyClassBranch");
  }
}

// GNAM
/*
  "end" function: called once just before the library will be unloaded.
  You should put here your final clean-up tasks.
*/
extern "C" void
end (void)
{
    ERS_DEBUG (2, "Starting");
    ERS_DEBUG (2, "Deleting MyObject");
    delete MyObject;
}

// GNAM
/*
  "decode" function: here you should analyze the data
  and fill the shared objects 
*/
extern "C" void
decode (const std::vector<uint32_t const *> *rods,
        const std::vector<unsigned long int> *sizes,
        const uint32_t *event, unsigned long int event_size)
{
    ERS_DEBUG (2, "Starting");
    if (rods->size() > rodSizeCheck) {
        MyObject->SetValue (rods->size(), (*sizes)[rodSizeCheck]);
    } else {
        MyObject->SetValue( rods->size(), -1);
    }
}


