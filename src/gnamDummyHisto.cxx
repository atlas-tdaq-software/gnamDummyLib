//$Id$

// GNAM: global include file for libraries
#include <gnam/gnamutils/GnamUtils.h>

// TDAQ: OKS DB entry point
#include <config/Configuration.h>

// TDAQ: for DB object casting
#include <dal/util.h>

// MY CLASS: definition of the class correspoding to my event
#include "gnamDummyLib/MyClass.h"

// MY DAL: definition of the DB object correspoding to my library
#include "gnamDummyLibdal/gnamDummyHistoLib.h"

// MY CONFIGURATION: class used internally to store the configuration
#include "gnamDummyLib/HistoConf.h"

// MY EXCEPTION: definitions of exceptions
#include "gnamDummyLib/GnamDummyException.h"

#include <stdlib.h>
#include <errno.h>

// GNAM
/*
  Use *one* instance for each one of your data histograms; their addresses
  will be added to a list after calling "bookHisto" and the list cannot
  change until next run.
*/
GnamHisto *rodNumDist, *rodSizeDist, *pao1h1, *pao1h2, *ann1, *ann2;
HistoConf * conf;
GnamHisto *hbuf1, *hbuf2, *hhist;

// Use this sintax for the histogram list. This will assure 
// a clean library unloading. Look at the sintax also in
// the "internal_init" and "end" functions  
static std::vector<GnamHisto*> *histolist = NULL;

// GNAM
/*
  "initDB" function: called once just after the library has been loaded
  in case of configuration from OKS DB.
  You should collect your configuration parameters from the DB.
*/
extern "C" void
initDB (Configuration * confDB, const gnamdal::GnamLibrary * library)
{
    ERS_DEBUG (2, "Starting");
    // In order to read your configuration parameters, you 
    // need to downcast the abstract object "library" to 
    // the type of your library 
    const gnamDummyLibdal::gnamDummyHistoLib * dummyLib =
        confDB->cast<gnamDummyLibdal::gnamDummyHistoLib,
        gnamdal::GnamLibrary>(library);
  
    conf = new HistoConf();
  
    conf->SetValue(dummyLib->get_RodSizeDistEnabled(),
                   dummyLib->get_RodNumDistLowerEnd(),
                   dummyLib->get_RodNumDistUpperEnd(),
                   dummyLib->get_RodNumDistBins());
  
    ERS_DEBUG (2, "ROD Size Histo Enable: " << conf->isRodSizeDistEnabled());
    ERS_DEBUG (2, "ROD Number Histo lower end: "<< conf->GetLowerEnd());
    ERS_DEBUG (2, "ROD Number Histo upper end: " << conf->GetUpperEnd());
    ERS_DEBUG (2, "ROD Number Histo bins: " << conf->GetBins());
}

// GNAM
/*
  "end" function: called once just before the library will be unloaded.
  You should put here your final clean-up tasks.
*/
extern "C" void
end (void)
{
    ERS_DEBUG (1, "Starting");
    if (histolist != NULL) {
        size_t k, max;
        max = histolist->size();
        for (k = 0; k < max; k++) {
            delete (*histolist)[k];
        }
        delete histolist;
    }
}

// GNAM
/*
  "fillHisto" function: look for your branch (or branches)
  in the data storage and fill your histograms.
*/
extern "C" void
fillHisto (void)
{
    ERS_DEBUG (2, "Starting");

    // GNAM
    /*
      "object" will point to the contents of the branch "MyClassBranch",
      which was filled by the decoding routine.
      For efficiency reasons, it is recommended to declare it static.
    */
    static const MyClass *object = NULL;
    try {
        GNAM_BRANCH_FILL ("MyClassBranch", MyClass, object);
    }
    catch (daq::gnamlib::NotExistingBranch &exc) {
        throw daq::gnamlib::CannotFindBranch (ERS_HERE, "MyClassBranch");
    }
  
    rodNumDist->Fill(object->GetRodNum());
  
    if(conf->isRodSizeDistEnabled())
        rodSizeDist->Fill(object->GetRodSize());
  
    pao1h1->Fill(10);
    pao1h2->Fill(5);
    ann1->Fill(5);
    ann2->Fill(5);

    // histogram buffer test
    static int iter = 0;
    hbuf1->Fill (iter);
    hbuf2->Fill (10 + (iter-10)/2);
    hhist->Fill (iter);
    iter ++;
    if (iter == 20) {
        iter = 0;
    }
}

// GNAM
/*
  "startOfRun" function: called once every start of run;
  here you can prepare your library for the histogram filling.
  "run" is the run number.
*/
extern "C" const std::vector<GnamHisto*> *
startOfRun (int run,std::string type)
{
    ERS_DEBUG (2, "Starting");
        ERS_DEBUG (2, "Starting");
    ERS_ASSERT (histolist == NULL);
    // Use this sintax
    histolist = new std::vector<GnamHisto *>;
  
    rodNumDist = new GnamHisto("/SHIFT/gnamDummyLib/rodN",
                               "Rod Number Distribution", 
                               conf->GetBins(), conf->GetLowerEnd(), 
                               conf->GetUpperEnd());
    histolist->push_back(rodNumDist);
  
  
    // Default bin type is short. You can change this behaviour 
    // using GnamHisto::DOUBLEBIN or GnamHisto::FLOATBIN or
    // GnamHisto::INTBIN
    rodSizeDist = new GnamHisto("/EXPERT/gnamDummyLib/rodS",
                                "Rod Size Distribution", 
                                1001, -1.5, 999.5,GnamHisto::FLOATBIN);
  
    // With these setter methods you can change the core behaviour 
    // for an histogram. There are also the getter methods:
    // IsPublished(), IsDumped(), IsFilled()
    rodSizeDist->IsPublished(conf->isRodSizeDistEnabled());
    rodSizeDist->IsDumped(conf->isRodSizeDistEnabled());
    rodSizeDist->IsFilled(conf->isRodSizeDistEnabled());
  
    // You can use this flag for low statistic histograms
    // Default is true
    // rodSizeDist->IsResetAtSOR(false);
  
    histolist->push_back(rodSizeDist);
  
  
    // Histograms belonging to the same PAO will 
    // be always published together 
    pao1h1= new GnamHisto ("/SHIFT/gnamDummyLib/pao1h1","first pao1 histo", 
                           20, -0.5, 19.5,"PAO1");
    
    pao1h2= new GnamHisto ("/SHIFT/gnamDummyLib/pao1h2","second pao1 histo", 
                           10, -0.5, 9.5,"PAO1",GnamHisto::DOUBLEBIN);
  
    histolist->push_back(pao1h1);
    histolist->push_back(pao1h2);
  
  
    ann1 = new GnamHisto("/EXPERT/gnamDummyLib/ann1",
                         "Histo with annotations 1", 41, -0.5, 40.5);
  
    // you can modify the annotations of an already created histo 
    // (even if it has been created without annotations)
    ann1->getAnnotation().push_back(std::pair<std::string,std::string>
                                    ("ann_name","ann_value"));
  
    // you can build a set of annotations and then create the histo  
    std::vector< std::pair<std::string,std::string> > ann;
    ann.push_back(std::pair<std::string,std::string>
                  ("ann_name1", "ann_value1"));
    ann.push_back(std::pair<std::string,std::string>
                  ("ann_name2", "ann_value2"));
    ann2 = new GnamHisto("/SHIFT/gnamDummyLib/ann2",
                         "Histo with annotations 2", 21, -0.5, 20.5,
                         GnamHisto::DOUBLEBIN, ann);	
  
    histolist->push_back(ann1);
    histolist->push_back(ann2);
  
    /*
     * Histogram buffer
     */
    hbuf1 = new GnamHisto("/SHIFT/gnamDummyLib/hbuf1","Histo buffer test 1",
                          21, -0.5, 20.5, GnamHisto::FROMHISTORY,
			  oh::util::EmptyAnnotation,120);
    histolist->push_back (hbuf1);

    hbuf2 = new GnamHisto("/SHIFT/gnamDummyLib/hbuf2","Histo buffer test 2",
                          21, -0.5, 20.5);
    hbuf2->FromHistory(true, 100);
    histolist->push_back (hbuf2);

    hhist = new GnamHisto("/SHIFT/gnamDummyLib/hhist","History histo test",
                          50, 0., 50., GnamHisto::INTBIN|GnamHisto::ASHISTORY);
    histolist->push_back (hhist);
    
    return histolist;
}

// GNAM
/*
  "prePublish" function: here you can fill special histograms just
  before the publication.
*/
extern "C" void
prePublish (bool endOfRun)
{
    ERS_DEBUG (2, "Starting");
  
    if (endOfRun) {
        ERS_DEBUG (0, "entrypoint called at end of run");
    }
}

// GNAM
/*
  "endOfRun" function: called once every end of run,
  here you make some clean-up
*/
extern "C" void
endOfRun (void)
{
    ERS_DEBUG (2, "Starting");
    ERS_ASSERT (histolist != NULL);
    size_t k, max;
    max = histolist->size();
    for (k = 0; k < max; k++) {
        delete (*histolist)[k];
    }
    delete histolist;
    histolist = NULL;
}


// GNAM
/*
  "DFStopped" function: this function is called after endOfRun, when 
  the DataFlow is completely stopped. You shoudl not update your
  histograms here since they aer not going to be published by Gnam. 
  The function is here mainly for MonaIsa support (IS gathering after the
  end of run).
*/
extern "C" void
DFStopped (void)
{
  ERS_DEBUG(2,"Starting");
}


// GNAM
/*
  "customCommand" function: here you will receive custom command from OH.
  You will have also the parameters of your command 
*/
extern "C" void
customCommand (const GnamHisto * histo, int argc, const char * const *argv)
{
    ERS_DEBUG (2, "Starting");
    for (int i = 0; i < argc; i++) {
        ERS_DEBUG (2, "Custom command. Argument " << i << ": "<< argv[i]);
    }
}
