//$Id$

#include "gnamDummyLib/HistoConf.h"

HistoConf::HistoConf()
{

}

void HistoConf::SetValue(bool aRodSizeDistEnabled,
			 Double_t aLowerEnd,
			 Double_t aUpperEnd,
			 Int_t aBins)
{
  rodSizeDistEnabled=aRodSizeDistEnabled;
  rodNumDistLowerEnd=aLowerEnd;
  rodNumDistUpperEnd=aUpperEnd;
  bins=aBins;
}
 
bool HistoConf::isRodSizeDistEnabled() const
{
  return rodSizeDistEnabled;
}
 
Double_t HistoConf::GetLowerEnd() const
{
  return rodNumDistLowerEnd;
}
 
Double_t HistoConf::GetUpperEnd() const
{
  return rodNumDistUpperEnd;
}

Int_t HistoConf::GetBins() const
{
  return bins;
}
