//$Id$

#include "gnamDummyLib/MyClass.h"

MyClass::MyClass()
{
}

MyClass::~MyClass()
{
}

void MyClass::SetValue (int aRodNum, int aRodSize) 
{ 
  rodNum = aRodNum; 
  rodSize = aRodSize;
}

int MyClass::GetRodSize() const
{
  return rodSize;
}

int MyClass::GetRodNum() const
{
  return rodNum;
}

