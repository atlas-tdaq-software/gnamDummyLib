//$Id$

#ifndef GNAM_DUMMY_EXCEPTION
#define GNAM_DUMMY_EXCEPTION

#include "gnam/gnamutils/GnamLibException.h"


namespace daq{

  ERS_DECLARE_ISSUE_BASE (gnamDummyLib, 
			  TestException, 
			  gnamlib::LibraryUnusableBase, 
			  "This is a test exception: " << message,
			  , 
			  ((std::string) message))
}

#endif 
