//$Id$

#ifndef __HistoConf__
#define __HistoConf__

#include <Rtypes.h>

class HistoConf
{
 private:
  bool rodSizeDistEnabled;
  Double_t  rodNumDistLowerEnd;
  Double_t  rodNumDistUpperEnd;
  Int_t bins;
 public:
  HistoConf();
  
  void SetValue(bool aRodSizeDistEnabled,
		Double_t aLowerEnd,
		Double_t aUpperEnd,
		Int_t aBins);
  bool isRodSizeDistEnabled() const;
  Double_t GetLowerEnd() const;
  Double_t GetUpperEnd() const;
  Int_t GetBins() const;
};


#endif
