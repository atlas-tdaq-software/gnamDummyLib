//$Id$

#ifndef __MyClass__
#define __MyClass__

class MyClass
{
 private:
  int rodSize;
  int  rodNum;
 public:
  MyClass();
  ~MyClass();
  void SetValue (int aRodNum, int aRodSize);
  int GetRodSize() const;
  int GetRodNum() const;
};

#endif




